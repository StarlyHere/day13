Wordle

Rules For FeedBack:

Each guess must be a valid 5-letter word.
The feedback tells you how close your guess was to the word.

RED: if letter not in word <br>
Yellow: letter in word but at incorrect position <br>
GREEN: letter in word and at correct position <br>