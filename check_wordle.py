import requests as rq
import random


class WordleBot:
    words = [word.strip() for word in open("5letters.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    create_url = mm_url + "create"
    guess_url = mm_url + "guess"
    SINGLETON = True
    MAX_ATTEMPTS = 6

    def __init__(self, name: str):
        if WordleBot.SINGLETON:
            WordleBot.SINGLETON = False
            self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(WordleBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']

    def setup_game(self):
        self.attempts = 0
        create_dict = {"id": self.me, "overwrite": True}
        self.session.post(WordleBot.create_url, json=create_dict)
        self.choices = WordleBot.words[:]
        random.shuffle(self.choices)

    def play(self) -> dict:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(WordleBot.guess_url, json=guess)
            rj = response.json()
            feedback = rj["feedback"]
            status = "win" in rj["message"]
            return feedback, status

        choice = random.choice(self.choices)
        feedback, won = post(choice)
        tries = [f'{choice}:{feedback}']
        print(tries)

        while not won and self.attempts < WordleBot.MAX_ATTEMPTS - 1:
            self.update(choice, feedback)
            if self.choices:
                choice = self.choices[0]  # Pick the next best option consistently
                feedback, won = post(choice)
                tries.append(f'{choice}:{feedback}')
            self.attempts += 1

        return {"Secret": choice, "Attempts": len(tries), "Route": " => ".join(tries)}

    def update(self, choice: str, feedback: str):
        def match_feedback(word: str, choice: str, feedback: str) -> bool:
            for i in range(5):
                if feedback[i] == 'G' and word[i] != choice[i]:
                    return False
                if feedback[i] == 'Y' and (word[i] == choice[i] or choice[i] not in word):
                    return False
                if feedback[i] == 'R' and choice[i] in word:
                    return False
            return True

        self.choices = [w for w in self.choices if match_feedback(w, choice, feedback)]

DEBUG = False
bot = WordleBot("CodeShifu")
for _ in range(10):
    bot.setup_game()
    print(bot.play())
