import requests as rq
import json


base_url = "https://we6.talentsprint.com/wordle/game/"
register_url = base_url + "register"
create_url = base_url + "create"
guess_url = base_url + "guess"


id = ''
session = rq.Session()


if not id:
    register_dict = {"mode": "wordle", "name": "Player"}
    reg = session.post(register_url, json=register_dict)
    response_data = reg.json()
    if "id" in response_data:
        id = response_data["id"]
    else:
        raise Exception("Failed to register player. Response: " + reg.text)


create_dict = {"id": id, "overwrite": True}
create_response = session.post(create_url, json=create_dict)
print("Create Response:", create_response.text)


guess_dict = {"id": id, "guess": "hello"}
guess_response = session.post(guess_url, json=guess_dict)
print("Guess Response:", guess_response.text)

def load_data(file_name: str) -> list:
    with open(file_name) as f:
        words = [line.strip() for line in f if len(line.strip()) == 5]
    return words

for i in range(5):
    word_list = load_data("day13/words.txt")
    for word in word_list:
        guess_dict = {"id": id, "guess": word}
        guess_response = session.post(guess_url, json=guess_dict)
        print(f"Guessing '{word}': {guess_response.text}")
        break
print("ATTEMPTS OVER \n")